package com.wirelessscanner.callback;

import android.os.Build;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.wirelessscanner.models.DTOTelephony;

import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.Q)
public class CellInfoClbck extends TelephonyManager.CellInfoCallback {

    private DTOTelephony dto;

    public CellInfoClbck(DTOTelephony dto){
        this.dto = dto;
    }

    @Override
    public void onCellInfo(@NonNull List<CellInfo> cellInfo) {
        this.dto.CellInfo = cellInfo;
        this.dto.onCellsLoaded.emit(this.dto);
    }
}
