package com.wirelessscanner.dao;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Build;

import com.wirelessscanner.models.DTOWiFi;

public class DAOWiFi {

    private Context c;
    private WifiManager wifiManager;

    public DAOWiFi(Context c){
        this.c = c;
        this.wifiManager = (WifiManager) c.getSystemService(Context.WIFI_SERVICE);
    }

    public DTOWiFi getInfos(){
        DTOWiFi data = new DTOWiFi();

        data.ScanResults = this.wifiManager.getScanResults();
        data.IsWifiEnabled = this.wifiManager.isWifiEnabled();
        data.ConnectionInfo = this.wifiManager.getConnectionInfo();
        data.DhcpInfo = this.wifiManager.getDhcpInfo();
        data.WiFiState = this.wifiManager.getWifiState();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            data.Is5GhzSupported = this.wifiManager.is5GHzBandSupported();
        }
        return data;
    }
}
