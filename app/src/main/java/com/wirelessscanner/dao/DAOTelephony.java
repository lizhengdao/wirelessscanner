package com.wirelessscanner.dao;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Debug;
import android.telephony.AvailableNetworkInfo;
import android.telephony.TelephonyManager;
import android.telephony.TelephonyScanManager;
import android.telephony.TelephonyScanManager.NetworkScanCallback;
import android.telephony.cdma.CdmaCellLocation;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.wirelessscanner.callback.CellInfoClbck;
import com.wirelessscanner.helper.HiddenHelper;
import com.wirelessscanner.models.DTOTelephony;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class DAOTelephony {

    private Context c;
    private TelephonyManager telManager;

    public DAOTelephony(Context context) {
        this.c = context;
        this.telManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
    }

    public DTOTelephony GetInfos() {
        if (Build.VERSION.SDK_INT >= 5)
            return this.GetDeprInfos();
        else
            return this.GetDeprInfos();
    }

    @SuppressLint("MissingPermission")
    private DTOTelephony GetDeprInfos(){
        DTOTelephony data = new DTOTelephony();


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1 ||
                hasPrivileges()) {
            data.IMEINumber = this.telManager.getDeviceId();
            data.subscriberID = this.telManager.getSubscriberId();
            data.SIMSerialNumber = this.telManager.getSimSerialNumber();
        }

        data.networkCountryISO = this.telManager.getNetworkCountryIso();
        data.SIMCountryISO = this.telManager.getSimCountryIso();
        data.SimOperator = this.telManager.getSimOperator();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            data.CellInfo = this.telManager.getAllCellInfo();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            data.DataNetworkType = this.telManager.getDataNetworkType();
            data.VoiceNetworkType = this.telManager.getVoiceNetworkType();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            this.telManager.requestCellInfoUpdate(this.c.getMainExecutor(), new CellInfoClbck(data));
        }


        if (ActivityCompat.checkSelfPermission(this.c, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            data.softwareVersion = this.telManager.getDeviceSoftwareVersion();
            data.voiceMailNumber = this.telManager.getVoiceMailNumber();
        }

        if (telManager.getCellLocation() != null && telManager.getCellLocation().getClass() == CdmaCellLocation.class)
            data.CellLoc = (CdmaCellLocation)this.telManager.getCellLocation();

        return data;
    }

    private boolean hasPrivileges(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            return this.telManager.hasCarrierPrivileges();
        }
        else
            return false;
    }

}
