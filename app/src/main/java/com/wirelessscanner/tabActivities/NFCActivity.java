package com.wirelessscanner.tabActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Toast;
import be.appfoundry.nfclibrary.activities.NfcActivity;
import com.wirelessscanner.R;
import com.wirelessscanner.controller.TelController;

public class NFCActivity extends NfcActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

    }
    public void startRead(View view){
        for (String message : getNfcMessages()){
            Toast.makeText(this, message, Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onNewIntent(Intent intent){
        super.onNewIntent(intent);
        for (String message : getNfcMessages()){
            Toast.makeText(this, message, Toast.LENGTH_SHORT);
            Log.d("NFC", message);
        }
    }
}
