package com.wirelessscanner.tabActivities;

import android.app.Activity;
import android.app.TabActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import com.wirelessscanner.R;
import com.wirelessscanner.controller.TelController;

public class TelActivity extends Activity {

    private TelController telController;

    public TelActivity(){

        super();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tel);

        this.telController = new TelController(this);
        this.telController.load();
    }

    public void showCellOnMap(View view){
        this.telController.showCellOnMap();
    }
}
