package com.wirelessscanner.tabActivities;

import android.app.Activity;
import android.os.Bundle;

import com.wirelessscanner.R;
import com.wirelessscanner.controller.WiFiController;

public class WiFiActivity extends Activity {

    private WiFiController wifiController;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi);

        this.wifiController = new WiFiController(this);
        this.wifiController.load();
    }
}
