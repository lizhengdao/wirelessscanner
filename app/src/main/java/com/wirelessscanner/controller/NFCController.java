package com.wirelessscanner.controller;

import android.app.Activity;

import android.nfc.NfcAdapter;
import com.wirelessscanner.models.DTONFC;

public class NFCController {

    private Activity ac;
    private NfcAdapter nfc;

    public NFCController(Activity ac){
        this.ac = ac;
        this.nfc = NfcAdapter.getDefaultAdapter(ac);
    }

    public DTONFC load(){
        DTONFC dto = new DTONFC();
        if (this.nfc == null)
            return dto;
        if (!this.nfc.isEnabled())
            return dto;

        return dto;
    }
}
