package com.wirelessscanner.controller;

import android.content.Context;
import android.util.Pair;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.wirelessscanner.R;
import com.wirelessscanner.models.DTOTelephony;

import java.util.ArrayList;

public class GridViewController {

    private Context c;
    private GridView gv;
    private ArrayAdapter<String> tel_adap;

    public GridViewController(GridView gv, Context context){
        this.gv = gv;
        this.c = context;
        this.setupGrid();
    }

    private void setupGrid(){
        ArrayList<String> itmlist = new ArrayList<String>();
        tel_adap = new ArrayAdapter<String>(this.c, R.layout.support_simple_spinner_dropdown_item, itmlist);
        gv.setAdapter(tel_adap);
    }

    public void addInfo(String name, String value){
        if (name == null || value == null)
            return;
        tel_adap.add(name);
        tel_adap.add(value);
    }

    public void clearInfo(){
        try{tel_adap.clear();}
        catch (Exception e) {}
    }
}
